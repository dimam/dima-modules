/*
 * Public API Surface of dima-error-dialog
 */
export * from './lib/dima-error-dialog.enums';
export * from './lib/dima-error-dialog.model';
export * from './lib/dima-error-dialog.component';
export * from './lib/dima-error-dialog.module';
