import { NgModule } from '@angular/core';
import { DimaErrorDialogComponent } from './dima-error-dialog.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
  ],
  declarations: [
    DimaErrorDialogComponent,
  ],
  exports: [DimaErrorDialogComponent]
})
export class DimaErrorDialogModule { }
