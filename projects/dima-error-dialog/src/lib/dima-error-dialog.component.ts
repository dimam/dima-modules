import {AfterViewInit, Component, Input, OnInit} from '@angular/core';

import * as DED from './dima-error-dialog.enums';
import * as $_ from 'jquery';
import {IDEDObject} from './dima-error-dialog.model';

const $ = $_;

@Component({
  selector: 'dima-error-dialog',
  templateUrl: './dima-error-dialog.component.html',
  styleUrls: ['./dima-error-dialog.component.scss']
})
export class DimaErrorDialogComponent implements AfterViewInit {

  STYLE_CLASSES = {
    error: 'error-color',
    info: 'info-color',
    success: 'success-color',
    warning: 'warning-color'
  };

  @Input() dem_object: IDEDObject;

  div_id = '#err-msg-back-div';

  constructor() {
  }

  ngAfterViewInit() {
    this.onResize();
    window.onresize = this.onResize;
  }

  show = (showDuration?: number, animationDuration?: number) => {
    animationDuration = animationDuration === undefined ? 200 : animationDuration;
    $(this.div_id).show(animationDuration);
    if (showDuration > 0) {
      setTimeout(() => {
        if ($(this.div_id).css('display') !== 'none') {
          $(this.div_id).hide(animationDuration);
        }
      }, showDuration * 1000);
    }
  };

  hide = (animationDuration?: number) => {
    animationDuration = animationDuration === undefined ? 200 : animationDuration;
    $(this.div_id).hide(animationDuration);
  };

  onResize = () => {
    switch (this.dem_object.position) {
      case DED.Positions.right: {
        $(this.div_id).css('right', 20);
        break;
      }
      case DED.Positions.center_top: {
        this.setCenter();
        break;
      }
      case DED.Positions.left: {
        $(this.div_id).css('left', 20);
        break;
      }
      case DED.Positions.center: {
        this.setCenter();
        $(this.div_id).css('top', window.innerHeight / 2 - $(this.div_id).height() / 2);
        break;
      }
      default: {
        this.setCenter();
        break;
      }
    }
  };

  setCenter() {
    if (window.innerWidth <= 600) {
      $(this.div_id).css('left', window.innerWidth * 0.05);
    } else if (window.innerWidth <= 900) {
      $(this.div_id).css('left', window.innerWidth * 0.15);
    } else {
      $(this.div_id).css('left', window.innerWidth * 0.20);
    }
  }

}
