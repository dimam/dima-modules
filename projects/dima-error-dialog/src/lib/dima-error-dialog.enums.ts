export enum Positions {
  left = 'left',
  center_top = 'center-top',
  right = 'right',
  center = 'center-all'
}

export enum Types {
  error = 'error',
  info = 'info',
  success = 'success',
  warning = 'warning',
}
