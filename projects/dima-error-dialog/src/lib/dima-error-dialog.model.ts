import * as DED from './dima-error-dialog.enums';

/** Model for the DEM-Component */
export class IDEDObject {
  constructor(
    public title = '',
    public message = '',
    public extra_message = '',
    public type = DED.Types.info,
    public position = DED.Positions.center
  ) {}
}
